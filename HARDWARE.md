# Hardware supported by Debian in a box

You need a supported computer:

  * [A20-OLinuXino LIME]
  * [A20-OLinuXino LIME2]
  * [A20-OLinuXino MICRO]
  * [A64-OLinuXino]
  * [SheevaPlug]
  * [Teres-I A64]

For devices with a dedicated power plug,
avoid the less stable USB-OTG connection
(even if it seems to work at first).

You also need a microSD card as install medium.
Any MicroSD card of 1 GB should work.
Recommended is a MicroSD card of 32 GB or larger in speed category A1 or A2.

You will need internet access during install,
both to get the install image
and to finalize system setup.
Most wifi chips require non-free drivers.
Recommended is to use a wired ethernet connection.

## Recommendations:

For a **workstation**,
use a [TERES-I A64],
a [ThinkPad Ethernet Adapter],
and any 1 GB MicroSD card (used only during install).

For a **storage server**,
use an [A20-OLinuXino LIME2] with eMMC
and dedicated power supply,
any 1 GB MicroSD card (used only during install),
and a SATA-connected harddisk.

For an **application server**,
use an [A64-OLinuXino] with eMMC
and dedicated power supply,
and any 1 GB MicroSD card (used only during install).

For a **gateway**,
use an [A20-OLinuXino LIME2] with eMMC
and dedicated power supply,
any 1 GB MicroSD card (used only during install),
and a [ThinkPad Ethernet Adapter].

Or use a box without eMMC
and a 32 GB MicroSD card in speed category A1 or A2
(regardless if you need that much storage:
That size onwards has a slight speed boost).

[A20-OLinuXino LIME]: <https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME/>
  "Olimex A20-OLinuXino LIME"

[A20-OLinuXino LIME2]: <https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME2/>
  "Olimex A20-OLinuXino LIME2"

[A20-OLinuXino MICRO]: <https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-MICRO/>
  "Olimex A20-OLinuXino MICRO"

[A64-OLinuXino]: <https://www.olimex.com/Products/OLinuXino/A64/A64-OLinuXino/>
  "Olimex A64-OLinuXino"

[ThinkPad Ethernet Adapter]: <https://support.lenovo.com/dk/en/solutions/pd029741>
  "Lenovo ThinkPad USB 3.0 Ethernet Adapter"

[SheevaPlug]: <https://www.globalscaletechnologies.com/p-46-sheevaplug-dev-kit.aspx>
  "GlobalScale SheevaPlug"

[TERES-I A64]: <https://www.olimex.com/Products/DIY-Laptop/>
  "Olimex TERES-I A64"

Jonas Smedegaard <dr@jones.dk>  Sun, 27 Sep 2020 17:36:37 +0200
