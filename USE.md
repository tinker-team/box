# Debian in a box

> Installing a custom-tailored Debian system

This documents how you can install a "box" system image
by essentially doing the following:

 1. Download a suitable image
    and load it onto the target computer.
 2. Start the computer,
    log in and personalize,
    and maybe include some addons.

Result is a pure Debian system
(not a fork nor a fixated snapshot).


## Prerequisites

You need [a supported computer, a microSD card, and an ethernet access][hardware].

[hardware]: <HARDWARE>
  "Hardware requirements for Debian in a box"


## Choose image

Each image is tailored for a specific computer,
and multiple revisions may be available.
Choose the newest revision for your computer
(or try an older revision if that doesn't work -
and [tell us][] about the problems you experienced).

The images are all freely licensed software,
except "nonfree" images which are licensed less freely.
Please check that legal risks are acceptable to you before using those!

[tell us]: <https://wiki.debian.org/DebianTinker>
  "Debian Tinker Team"


## Download image

Download newest [Lime2 image] or [Teres-I image]
(or browse through [all images]).

Recommended approach is to use
one of the command-line tools aria, curl, or wget.

With **aria**,
use this command
(adapt URL to match your image):

    aria2c https://box.redpill.dk/images/core/core-lime2-buster.img.gz

With **curl**,
use this command
(adapt URL to match your image):

    curl --remote-name https://box.redpill.dk/images/core/core-lime2-buster.img.gz

With **wget**,
use this command
(adapt URL to match your image):

    wget https://box.redpill.dk/images/core/core-lime2-buster.img.gz

With **rsync**,
use this command
(adapt URL to match your image):

    rsync rsync://box.redpill.dk/images/core/core-lime2-buster.img.gz .

[Lime2 image]: <images/core/core-lime2-buster.img.gz>
  "Debian system image tailored for Olimex OLinuXino lime2 box"

[Teres-I image]: <images/core/core-teres1-buster.img.gz>
  "Debian system image tailored for Olimex Teres-I DIY laptop"

[all images]: <images/>
  "Debian system images tailored for various computers and purposes"


## Store image

You need to transfer the downloaded image onto your install device -
i.e. overwrite the whole raw **disk** device
(not a partition on the disk).

**NB!**
Take great care not to accidentally overwrite the wrong disk device!

First identify the **path** name of the install device
by running a command repeatedly -
with and without the install device plugged in -
to notice which device changes in the output of the command.

On **linux**,
try this command:

    lsblk --paths

On **macOS**,
try this command:

    ls /dev/rdisk?

The path is listed in the first column -
something like `/dev/sdf` or `/dev/rdisk7`.

Check also that the disk is not mounted,
on linux shown in last column of output of above command.
If the device disk has anything mounted
then **unmount** it before preceeding.

Decompress and copy image onto card
(adjust image name and device path as needed):

    gunzip core-lime2-buster.img.gz
    sudo cp core-lime2-buster.img PATH_TO_YOUR_SDCARD


## Boot system

Turn off the target computer,
plug in the microSD card,
turn on power,
and log in.

Username
: **debian**

Password
: **insecure**

You can log in either directly or via another computer,
when login service is ready (20-60 seconds).

With screen and keyboard **directly connected**,
log in at prompt.

From **another linux system** (requires avahi-utils),
look up hostname and login with ssh:

    avahi-browse --terminate _ssh._tcp
    ssh debian@HOSTNAME_LISTED_ABOVE.local

From a **macOS or Windows system**,
look up hostname (press CTRL+c to stop) and login with ssh:

    dns-sd -B _ssh._tcp
    ssh debian@HOSTNAME_LISTED_ABOVE.local


## Network setup

Ideally the system should have internet access during setup.
You can skip this part,
but are then recommended to revisit both this section and [general setup]
when system later gets online.

Check network access (status should ideally be _routable_):

    networkctl status

Optionally you can additionally test connectivity
towards a common public host:

    mtr --displaymode=1 1.1.1.1

With **ethernet**
(builtin or a USB dongle),
just connect cable.

With **wifi**,
do something like this:

    iwctl station wlan0 get-networks
    iwctl station wlan0 connect SSID_LISTED_ABOVE

If above fails with error message

> Operation failed

then try restart iwd:

    sudo service iwd restart

...and then repeat the `iwctl` commands.

<!-- TODO: show when broadband modem is supported in core images
With **broadband modem**,
do something like this:

    mmcli --modem=0 --simple-connect="apn=internet"

[general setup]: <#general-setup>
-->

### static IP

If a network connection requires fixed IP address,
first do the appropriate setup above
and check the link name of the device:

    networkctl list

...and then do something like this
(adapt to replace "eth0" with link name in above listing):

    cp /etc/systemd/network/90-dhcp.network /etc/systemd/network/10-eth0.network
    nano /etc/systemd/network/10-eth0.network
    # Replace line "Name=..." with line "Name=eth0"
    # Add lines "Address=..." and "Gateway=..." and "DNS=..." as needed
    # Remove line "DHCP=..."
    # Quit nano with CTRL+x and agree to save changes under proposed name
    service systemd-networkd restart


## General setup

Finalize setup of the system:

    sudo system-setup

System is succesfully finalized when it ends with this message:

> Box setup configured succesfully!

Otherwise follow the instructions provided, if any,
or please consider report as a bug in the system-setup script.

## Addons

System is quite minimal,
but ships with several optional addons
all starting with "box-add-".

Examples are a (more complete) text-based user interface:

    sudo box-add-tui

...and a graphical desktop environment:

    sudo box-add-gui


## Source

These binary images are built from Debian packages
and build routines written by Jonas Smedegaard.
The build routines,
available at <https://salsa.debian.org/tinker-team/box>,
are licensed under the GNU General Public License, version 3 or newer.

When this text is accompagnied by binary system images,
the following applies to satisfy license requirements:

I hereby offer to provide the source code
for the relevant Debian binary packages, included in the installer,
on request.
However, you will probably find it easier to acquire these packages
from the official Debian resources,
<https://ftp.debian.org/> and/or <https://snapshot.debian.org/>.


Jonas Smedegaard <dr@jones.dk>  Thu, 27 Jun 2019 12:35:09 +0200
