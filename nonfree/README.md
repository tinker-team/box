# Box with non-free parts

These images are like regular images
except for the addition of non-free package "firmware-realtek"
to enable support for the builtin wifi.

Special licensing apply:
<https://metadata.ftp-master.debian.org/changelogs/non-free/f/firmware-nonfree/>
