# Pre-boxed Debian systems _(box)_

> Debian pre-configured system for curated set of computers

This project provides production ready Debian systems
tailored to specific computers.

The systems are carefully kept as close as possible to Debian
both regarding installed code and its configuration.
This helps ensure long-term maintainability,
without surprises for the local administrator
when software years later is upgraded.

The computers supported are selected
for being cheap and low-power while still reasonably usable,
friendly towards free software hacking,
and friendly towards free hardware hacking.


## Contributing

Feel free to dive in!

Make your proposed changes to a fork of the source git,
and [tell us][] where we can get your fork.
Or even better: Join our team :-)

[tell us]: <https://wiki.debian.org/DebianTinker>
  "Debian Tinker Team"


## Rights

Copyright © 2015-2019  Jonas Smedegaard <dr@jones.dk>

Code part of this work is free software;
you can redistribute it and/or modify it
under the terms of the [GNU General Public License][]
as published by the Free Software Foundation;
either version 3, or (at your option) any later version.

Markdown documents part of this work are licensed
under a Creative Commons Attribution-ShareAlike 4.0 International License
([CC-BY-SA-4.0][]).

When this README file is accompagnied by binary system images,
the following applies to satisfy license requirements:

I hereby offer to provide the source code
for the relevant Debian binary packages, included in the installer,
on request.
However, you will probably find it easier to acquire these packages
from the official Debian resources,
<https://ftp.debian.org/> and/or <https://snapshot.debian.org/>.

[GNU General Public License]: <https://www.gnu.org/licenses/gpl-3.0.html>
  "GNU General Public License"

[CC-BY-SA-4.0]: <https://creativecommons.org/licenses/by-sa/4.0/>
  "Creative Commons Attribution-ShareAlike 4.0 International license"
