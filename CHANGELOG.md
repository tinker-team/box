# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [1.1.12] - 2020-10-17

### Fixed

  * system-setup: fix detect device-tree paths for LIME2 boards

### Added

  * add node +gui_sway

### Changed

  * system-setup:
    force wipe pre-existing filesystem signatures when creating partitions
  * generate only one flavor of a64 using -mmc device-tree,
    and recommend it for application server
  * node core_a64: stop tell flash-kernel how to detect board:
    supported since flash-kernel 3.101 now in bullseye
  * generate lime2 images using -mmc device-tree,
    and generally recommend boards with eMMC

### Documentation

  * docs: generalize web page rendering
  * docs: move requirements to separate page hardware, and expand with recommendations

## [1.1.11] - 2020-09-27

### Changed

  * have box-add-* scripts initially emit currently available disk space
  * update node +gui_i3:
    include packages fonts-terminus stterm tmux

## [1.1.10] - 2020-07-28

### Fixed

  * system-setup:
    fix fixate rootfs when _not_ reinstalling (sigh...)
  * fix build image a64-emmc
    (accidentally used non-emmc recipe for tweaks)
  * system-setup:
    fix flash u-boot to new disk when reinstalling
    (flash-kernel only flashes u-boot config, not u-boot itself);
    thanks to Erik Josefsson

### Changed

  * system-setup:
    tidy: resolve reinstall partitioning type early
  * system-setup:
    initially allocate 4GB to root partition
    (not 3GB: 500MB too short for box-add-gui

## [1.1.9] - 2020-07-27

### Fixed

  * system-setup:
    fixate / before unmounting target disk when reinstalling;
    thanks to Siri Reiter

### Changed

  * system-setup:
    improve reinstall dialogs;
    thanks to Siri Reiter
  * include config snippets for apache2 and rsync services

### Documentation

  * fix install path for nonfree/README
  * USE:
    mention rsync access in section Download image
  * USE:
    mention what to expect of a succesful finalizing in USE

## [1.1.8] - 2020-07-26

### Fixed

  * system-setup:
    fix fixate swap and /home after copying fstab when reinstalling
    (not bogusly before);
    thanks to Anders Krøyer
  * system-setup:
    fix install arm-trusted-firmware only when needed

### Changed

  * system-setup:
    rename function fixate_bootloader_rootfs -> fixate_bootloader

## [1.1.7] - 2020-07-26

### Fixed

  * system-setup:
    resolve singular UUIDs (avoid UUIDs of descendant partitions);
    thanks to Siri Reiter
  * system-setup:
    re-install with disk encryption,
    by passing LUKS UUID to u-boot as needed (not always rootfs UUID);
    thanks to Siri Reiter
  * system-setup:
    avoid messing with finalized rootfs (and failing, when using LVM),
    and treat fixated rootfs as final (drop TODO notes)

### Changed

  * tidy warnings to not include line number
  * warn when make is called as root
  * system-setup:
    revert to setup locales before offer re-install,
    to ensure local keyboard layout when providing LUKS passphrase
  * system-setup:
    tidy generated fstab file
  * system-setup:
    fixate each mountpoint separately,
    updating both comment and mountpoint lines,
    or strip both if unused
  * system-setup:
    clarify initial request(s) for LUKS passphrase;
    thanks to Siri Reiter
  * system-setup:
    mention actual new disk when exiting after reinstall
    (not bogusly hardcoded 'internal disk')
  * system-setup:
    ask for confirmation of reinstall,
    warning about data loss
  * nodes core*:
    drop tweak to limit video buffers
    (seemingly frees memory only on obsolete non-mainline linux)

### Documentation

  * suggest restart iwd if failing; thanks to Siri Reiter
  * omit notes on enabling broadband modem: currently not included with core images
  * add nonfree/README

## [1.1.6] - 2020-07-23

### Fixed

  * system-setup:
    fix and generalize detecting if root partition is finalized
    (broken since v1.1.0 for both expansion and re-install)
  * system-setup:
    upgrade dependencies of locales together with locales;
    thanks to Erik Josefsson
  * system-setup:
    use UUID in flash-kernel setings (not only fstab)
  * system-setup:
    use stable LABEL identifiers for root and boot partitions;
    thanks to Karl Semich

### Changed

  * system-setup:
    resolve available target disks only if used,
    i.e. after checking network and root partition
  * system-setup:
    generalize fixating mountpoints
  * system-setup:
    avoid partition UUIDs on install media
  * system-setup:
    offer re-install before setting up locales

## [1.1.5] - 2020-07-21

### Fixed

  * system-setup:
    fix install rsync when reinstalling to another disk
  * system-setup:
    fix boot in Bullseye, via u-boot-menu updated to release 4.0.2

### Changed

  * system-setup:
    resize rootfs before tracking optional TOOLS
  * system-setup: check connectivity early,
    and skip reinstall question when offline
  * nodes core*: avoid busybox-static (not only busybox);
    thanks to Karl Semich

## [1.1.4] - 2020-07-11

### Fixed

  * fix build target to (re)make recipes
  * nodes +extroverted +introverted:
    fix handle service ssh (not bogus sshd)
  * node +gateway:
    fix enable dhcp service

### Added

  * support arm64 devices a64 a64-emmc

### Changed

  * nodes core*:
    add class hw.firmware.nic
    (and stop explicitly include package firmware-ath9k-htc);
    tighten dependency on boxer-data
  * nodes core*:
    include package fdisk explicitly
    (it is no longer a core package since Bullseye)
  * nodes core*:
    drop comments about package debian-security-support:
    included with Admin.apt.tools since boxer-data 10.8.15
  * support arch- and di-specific device lists;
    have di avoid a64-emmc;
    have buster avoid a64 a64-emmc

## [1.1.3] - 2020-04-06

### Fixed

  * node +solidbox: fix syntax of sagan tweak
  * fix access to root rescue shell,
    thanks to Smokeysea <smokeysea@protonmail.com> (see bug#802211);
    tighten core profiles to require Buster or newer
    (rescue shell fix requires systemd 240)
  * fix resolve when to use qemu
    (fakeroot mode still broken: now hangs...)
  * update workaround for ca-certificates broken with QEMU (bug#923479):
    apparently previous workaround fails with recent glibc or openssl

### Changed

  * nodes core*:
    include class Console.editor (not Admin.tools)
  * nodes core*:
    stop include package modemmanager
  * node +solidbox:
    include package alsa-utils
  * node +solidbox:
    include package esekeyd; add TODO comments
  * limit each rsync copy to same filesystem

## [1.1.2] - 2020-01-21

### Added

  * add node +domotics

### Changed

  * node +solidbox:
    include packages mpd mpd-sima prosody radicale

## [1.1.1] - 2019-12-09

### Fixed

  * system-setup:
    fix offer to reinstall
  * system-setup:
    fix setup keyboard before offer to reinstall
    (needed for providing passphrase for full disk encryption)
  * system-setup:
    fix ask initial debconf questions at default priority
    (i.e. suppress by default)
  * system-setup:
    fix handle unresolvable package status

### Added

  * add node +fixed

### Documentation

  * highlight fewer words,
    to avoid mistaking as subsection
  * fix drop lsblk option --nodeps,
    to show mountpoints
  * cover target hardware requirements in USE (not SETUP)
  * rewrite USE intro
  * rewrite USE section Stion image

## [1.1.0] - 2019-12-04

### Fixed

  * system-setup:
    Fix resize root partition in bullseye
    (use sfdisk not parted).
  * system-setup:
    Fix skip netwok actions as needed.

### Added

  * system-setup:
    Optionally reinstall to another storage device.

### Changed

  * Use apt-get (not apt) in non-interactive routines.

### Documentation

  * Misc. improvements

## [1.0] - 2019-11-05

## [1.0rc7] - 2019-09-28

## [1.0rc6] - 2019-09-04

## [1.0rc5] - 2019-06-27

## [1.0rc4] - 2019-06-24

## [1.0rc3] - 2019-06-17

## [1.0rc2] - 2019-06-04

## [1.0rc1] - 2019-05-05

## [1.0b22] - 2019-05-02

## [1.0b21] - 2019-04-25

## [1.0b20] - 2019-04-24

## [1.0b19] - 2019-04-23

## [1.0b18] - 2019-04-03

## [1.0b17] - 2018-12-28

## [1.0b16] - 2018-10-19

## [1.0b15] - 2018-10-14

## [1.0b14] - 2018-10-13

## [1.0b13] - 2018-09-13

## [1.0b12] - 2018-07-13

## [1.0b11] - 2018-04-17

## [1.0b10] - 2017-11-28

## [1.0b9] - 2017-11-11

## [1.0b8] - 2017-11-09

## [1.0b7] - 2017-02-23

## [1.0b4] - 2016-11-07

## [1.0b3] - 2016-11-06

## [1.0b2] - 2016-11-03

## [1.0b1] - 2016-11-03

## [0.1.0] - 2015-08-27

### Added

  * Initial experimental release.

[Unreleased]: https://salsa.debian.org/tinker-team/box/compare/v1.1.12...master
[1.1.12]: https://salsa.debian.org/tinker-team/box/compare/v1.1.11...v1.1.12
[1.1.11]: https://salsa.debian.org/tinker-team/box/compare/v1.1.10...v1.1.11
[1.1.10]: https://salsa.debian.org/tinker-team/box/compare/v1.1.9...v1.1.10
[1.1.9]: https://salsa.debian.org/tinker-team/box/compare/v1.1.8...v1.1.9
[1.1.8]: https://salsa.debian.org/tinker-team/box/compare/v1.1.7...v1.1.8
[1.1.7]: https://salsa.debian.org/tinker-team/box/compare/v1.1.6...v1.1.7
[1.1.6]: https://salsa.debian.org/tinker-team/box/compare/v1.1.5...v1.1.6
[1.1.5]: https://salsa.debian.org/tinker-team/box/compare/v1.1.4...v1.1.5
[1.1.4]: https://salsa.debian.org/tinker-team/box/compare/v1.1.3...v1.1.4
[1.1.3]: https://salsa.debian.org/tinker-team/box/compare/v1.1.2...v1.1.3
[1.1.2]: https://salsa.debian.org/tinker-team/box/compare/v1.1.1...v1.1.2
[1.1.1]: https://salsa.debian.org/tinker-team/box/compare/v1.1.0...v1.1.1
[1.1.0]: https://salsa.debian.org/tinker-team/box/compare/v1.0...v1.1.0
[1.0]: https://salsa.debian.org/tinker-team/box/compare/1.0rc7...v1.0
[1.0rc7]: https://salsa.debian.org/tinker-team/box/compare/1.0rc6...1.0rc7
[1.0rc6]: https://salsa.debian.org/tinker-team/box/compare/1.0rc5...1.0rc6
[1.0rc5]: https://salsa.debian.org/tinker-team/box/compare/1.0rc4...1.0rc5
[1.0rc4]: https://salsa.debian.org/tinker-team/box/compare/1.0rc3...1.0rc4
[1.0rc3]: https://salsa.debian.org/tinker-team/box/compare/1.0rc2...1.0rc3
[1.0rc2]: https://salsa.debian.org/tinker-team/box/compare/1.0rc1...1.0rc2
[1.0rc1]: https://salsa.debian.org/tinker-team/box/compare/1.0b22...1.0rc1
[1.0b22]: https://salsa.debian.org/tinker-team/box/compare/1.0b21...1.0b22
[1.0b21]: https://salsa.debian.org/tinker-team/box/compare/1.0b20...1.0b21
[1.0b20]: https://salsa.debian.org/tinker-team/box/compare/1.0b19...1.0b20
[1.0b19]: https://salsa.debian.org/tinker-team/box/compare/1.0b18...1.0b19
[1.0b18]: https://salsa.debian.org/tinker-team/box/compare/1.0b17...1.0b18
[1.0b17]: https://salsa.debian.org/tinker-team/box/compare/1.0b16...1.0b17
[1.0b16]: https://salsa.debian.org/tinker-team/box/compare/1.0b15...1.0b16
[1.0b15]: https://salsa.debian.org/tinker-team/box/compare/1.0b14...1.0b15
[1.0b14]: https://salsa.debian.org/tinker-team/box/compare/1.0b13...1.0b14
[1.0b13]: https://salsa.debian.org/tinker-team/box/compare/1.0b12...1.0b13
[1.0b12]: https://salsa.debian.org/tinker-team/box/compare/1.0b11...1.0b12
[1.0b11]: https://salsa.debian.org/tinker-team/box/compare/1.0b10...1.0b11
[1.0b10]: https://salsa.debian.org/tinker-team/box/compare/1.0b9...1.0b10
[1.0b9]: https://salsa.debian.org/tinker-team/box/compare/1.0b8...1.0b9
[1.0b8]: https://salsa.debian.org/tinker-team/box/compare/1.0b7...1.0b8
[1.0b7]: https://salsa.debian.org/tinker-team/box/compare/1.0b6...1.0b7
[1.0b4]: https://salsa.debian.org/tinker-team/box/compare/1.0b3...1.0b4
[1.0b3]: https://salsa.debian.org/tinker-team/box/compare/1.0b2...1.0b3
[1.0b2]: https://salsa.debian.org/tinker-team/box/compare/1.0b1...1.0b2
[1.0b1]: https://salsa.debian.org/tinker-team/box/compare/8a0c889...1.0b1
[0.1.0]: https://salsa.debian.org/tinker-team/box/commits/8a0c889
