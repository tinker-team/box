#!/bin/bash
#
# system-setup - finalize/reconfigure system setup
# Copyright 2016-2019 Jonas Smedegaard <dr@jones.dk>
# License: GNU Public License version 3 or newer
#
# Recommends: whiptail | dialog, crda

set -eu
export LC_ALL=C.UTF-8

_DEBCONF=$(debconf-show debconf d-i netcfg)
_DEBCONF_SEEN=$(echo "$_DEBCONF" | grep --perl-regexp --max-count=1 \
	--regexp='(?s)^\s*\*\s' || true)

# setup UI
DEBIAN_FRONTEND=${DEBIAN_FRONTEND:-$(\
	grep --perl-regexp --only-matching --no-filename --max-count=1 \
		--regexp='^Frontend:\s+\K\S+' \
		~/.debconfrc /etc/debconf.conf 2>/dev/null \
	echo "$_DEBCONF" | grep --perl-regexp --only-matching \
		--regexp='^\s*\*?\s+debconf/frontend:\s+\K.*' || true)}
DEBIAN_PRIORITY=${DEBIAN_PRIORITY:-$(\
	grep --perl-regexp --only-matching --no-filename --max-count=1 \
		--regexp='^Priority:\s+\K\S+' \
		~/.debconfrc /etc/debconf.conf 2>/dev/null \
	echo "$_DEBCONF" | grep --perl-regexp --only-matching \
		--regexp='^\s*\*?\s+debconf/priority:\s+\K.*' || true)}
if [ -z "$DEBIAN_FRONTEND" ] || [ -z "$DEBIAN_PRIORITY" ] || [ -z "$_DEBCONF_SEEN" ]; then
	dpkg-reconfigure --priority="${DEBCONF_PRIORITY:-high}" --unseen-only debconf
fi
DEBIAN_FRONTEND=${DEBIAN_FRONTEND:-$(\
	debconf-show debconf | grep --perl-regexp --only-matching \
		--regexp='^\s*\*?\s+debconf/frontend:\s+\K.*' || true)}
UI=tui
case "$DEBIAN_FRONTEND" in
  [Nn]oninteractive|[Tt]eletype)
	UI=
	;;
  [Rr]eadline)
	UI=cli
	;;
esac
dialog=$( \
	command -v ${DEBCONF_FORCE_DIALOG:-whiptail dialog} \
	| grep --max-count=1 --perl-regexp --only-matching --regexp='.*/\K\S+' || true)
[ -n "$dialog" ] || UI=cli
ui_yesno() {
	title=$1
	details=$2
	prompt=$3
	cliprompt=${4:-}
	yesbutton=${5:-}
	nobutton=${6:-}
	defaultno=${7:-}
	case "$UI" in
	  tui)
		$dialog --title "$title" \
			${yesbutton:+--yes-button "$yesbutton"} \
			${nobutton:+--no-button "$nobutton"} \
			${defaultno:+--defaultno} \
			--yesno -- "${details:+$details\n\n}$prompt" 0 0 \
			3>&1 1>&2 2>&3
		;;
	  cli)
		cliprompt=${cliprompt:-$(echo "$prompt" \
			| sed --regexp-extended \
				--expression='s/([?:])$/ (%s)\1/')}
		if [ -z "$defaultno" ]; then
			printf '%s%s (Y/n): ' "${details:+$details\n\n}" "$cliprompt" >&2
			read RET
			case "$RET" in [Yy]*|'') true;; *) false;; esac
		else
			printf '%s%s (y/N): ' "${details:+$details\n\n}" "$cliprompt" >&2
			read RET
			case "$RET" in [Nn]*|'') false;; *) true;; esac
		fi
		;;
	esac
}
ui_inputbox() {
	title=$1
	details=$2
	prompt=$3
	cliprompt=${4:-}
	default=${5:-}
	case "$UI" in
	  tui)
		$dialog --title "$title" \
			--inputbox -- "${details:+$details\n\n}$prompt" 0 0 \
			"$default" \
			3>&1 1>&2 2>&3
		;;
	  cli)
		if [ -n "$default" ]; then
			cliprompt=$(echo "${cliprompt:-$prompt}" \
				| sed --regexp-extended \
					--expression='s/([?:])$/ (%s)\1/')
		else
			cliprompt=${cliprompt:-$prompt}
		fi
		printf '%s%s (%s): ' "${details:+$details\n\n}" "$cliprompt" "$default" >&2
		read RET
		echo "$RET"
		;;
	esac
}
ui_radiolist() {
	title=$1
	details=$2
	prompt=$3
	list=$4
	default=${5:-}
	clicolumns=${6:-}
	cliprompt=${7:-}
	tuilistprepend=${8:-}
	tuilistappend=${9:-}
	case "$UI" in
	  tui)
		NL='
'
		fulllist=$(echo "${tuilistprepend:+$tuilistprepend$NL}$list${tuilistappend:+$NL$tuilistappend}" \
			| sed --expression='s/$/\tOff/' \
				--expression="\,^$default\t, s/Off\$/On/")
		_IFS="$IFS"
		IFS="	$NL"
		set -- $fulllist
		IFS="$_IFS"
		$dialog --title "$title" \
			--notags \
			--radiolist -- "${details:+$details\n\n}$prompt" 0 0 0 \
			"$@" \
			3>&1 1>&2 2>&3
		;;
	  cli)
		if [ -n "$default" ]; then
			cliprompt=$(echo "${cliprompt:-$prompt}" \
				| sed --regexp-extended \
					--expression='s/([?:])$/ (%s)\1/')
		else
			cliprompt=${cliprompt:-$prompt}
		fi
		echo "${clititle:-$title}" >&2
		echo "$list" \
			| column -t -s '	' \
			| pr --omit-header ${clicolumns:+--columns="$clicolumns"} >&2
		echo >&2
		printf '%s%s (%s): ' "${details:+$details\n\n}" "$cliprompt" "$default" >&2
		read RET
		echo "$RET"
		;;
	esac
}

rootfs_not_finalized() {
	grep --quiet --regexp="^LABEL=root\s" /etc/fstab
}

pkgstatus() {
	dpkg-query --show --showformat='${db:Status-Abbrev} ${Package} \n' -- "$@" 2>/dev/null || true
}

fixate_mountpoint() {
	if [ -n "$4" ]; then
		sed --in-place --regexp-extended \
			--expression="s,^# $2 was .*,# $2 was on $3 during installation," \
			--expression="s,^(/|LABEL=|UUID=)\S+(\s+$2\s),UUID=$4\2," \
			"${1}etc/fstab"
	else
		sed --in-place --regexp-extended \
			--expression="\,^# $2 was \,d" \
			--expression="\,^(/|LABEL=|UUID=)\S+(\s+$2\s)\,d" \
			"${1}etc/fstab"
	fi
}

fixate_bootloader() {
	dtmodel='/sys/firmware/devicetree/base/model'
	if [ -e /usr/lib/arm-trusted-firmware ] && which u-boot-install-sunxi64 2>/dev/null; then
		u-boot-install-sunxi64 "$2" 2>/dev/null
	elif [ -f "$dtmodel" ]; then
		case $(cat "${dtmodel}") in
			'Olimex A20-OLinuXino-LIME') TARGET=/usr/lib/u-boot/A20-OLinuXino-Lime/;;
			'Olimex A20-OLinuXino-LIME2') TARGET=/usr/lib/u-boot/A20-OLinuXino-Lime2/;;
			'Olimex A20-OLinuXino-LIME2-eMMC') TARGET=/usr/lib/u-boot/A20-OLinuXino-Lime2-eMMC/;;
			'Olimex A20-Olinuxino Micro') TARGET=/usr/lib/u-boot/A20-OLinuXino_MICRO/;;
		esac
		dd conv=notrunc bs=8k seek=1 if="${1}${TARGET}u-boot-sunxi-with-spl.bin" of="$2"
	fi
	if [ -f "${1}/etc/default/flash-kernel" ]; then
		sed --in-place --regexp-extended \
			--expression="s,root=\S+,root=UUID=${4:-$3},g" \
			"${1}/etc/default/flash-kernel"
		${1:+chroot "$1" }flash-kernel
	fi
	if [ -n "$4" ] && [ -f "${1}/etc/default/u-boot" ]; then
		sed --in-place --regexp-extended \
			--expression="s,^#?U_BOOT_ROOT=.*,U_BOOT_ROOT=UUID=$4,g" \
			"${1}/etc/default/u-boot"
		${1:+chroot "$1" }u-boot-update
	fi
}

# identify disks and partitions
disk=$(lsblk --paths --output NAME,TYPE,MOUNTPOINT --noheadings --list \
	| tac \
	| sed --quiet --expression='/\s\/$/,/\sdisk\s*$/ p' \
	| grep --perl-regexp --only-matching --regexp='^\S+(?=\s+disk\s*$)')
rootpart=$(lsblk --paths --output NAME,MOUNTPOINT --noheadings --list "$disk" \
	| grep --perl-regexp --only-matching --regexp='^\S+(?=\s+/$)')
bootpart=$(lsblk --paths --output NAME,MOUNTPOINT --noheadings --list "$disk" \
	| grep --perl-regexp --only-matching --regexp='^\S+(?=\s+/boot$)')

# extend root partition
if [ -n "$disk" ] && [ -n "$rootpart" ] && rootfs_not_finalized; then
	rootpartline=$(sfdisk --quiet --list --output=Device "$disk" \
		| tail --lines=+2 \
		| grep --line-number --fixed-strings --line-regexp --regexp="$rootpart" \
		| grep --perl-regexp --only-matching --regexp='^\d+')
	if [ -n "$rootpartline" ]; then
		echo "INFO: Extending partition \"$rootpart\" on disk \"$disk\"..." >&2
		echo '- +' | sfdisk --quiet --no-reread --no-tell-kernel -N "$rootpartline" "$disk"
		partx --update "$disk"
		resize2fs "$rootpart"
	fi
fi

# needed packages possibly not yet installed
TOOLS='busybox libc-l10n libgpg-error-l10n debconf-i18n e2fsprogs-l10n krb5-locales'

# check connectivity without much disk space demands (rootfs not yet resized)
SKIP_NET=
if [ -z "$SKIP_NET" ] && ! ip route list default | grep --quiet --regexp=^default; then
	# TODO: interactive UI to setup network
	if ! ui_yesno 'Configure the network' \
		'Network seems unusable.' \
		'Continue anyway?' \
		'' \
		'Continue' 'Cancel' \
		'1'
	then
		echo "Script cancelled. Please setup networking and try again." >&2
		exit 1
	fi
	echo "INFO: Continuing but skipping parts needing network access." >&2
	SKIP_NET=1
fi

[ -n "$SKIP_NET" ] || apt-get update

# TODO: check and skip TOOLS already installed and up-to-date

# setup locales
_LANG=$(grep --perl-regexp --only-matching \
	--regexp='^LANG=\K[a-z]{2,3}(?=_|\z)' /etc/default/locale || true)
_COUNTRY=$(grep --perl-regexp --only-matching \
	--regexp='^LANG=[^_]+_\K[A-Z]{2,3}(?!=[A-Z])' /etc/default/locale || true)
_LOCALE=$(grep --perl-regexp --only-matching \
	--regexp='^LANG=\K\S+' /etc/default/locale || true)
set -- $TOOLS
if [ -z "$SKIP_NET" ] && pkgstatus locales "$@" | grep --quiet --invert-match --regexp=^.i; then
	if case "$UI" in
	  tui)
		_LANG=$(ui_radiolist "Select a language" \
			'' \
			"Choose the default language for the system" \
			'C	No localization
other	[Install language support...]' \
			'other')
		[ 'other' = "$_LANG" ] && _LANG=
		;;
	  cli)
		ui_yesno 'Select a language' \
			'' \
			'Install language support?'
		;;
	esac
	then
		if pkgstatus "$@" | grep --quiet --invert-match --regexp=^.i; then
			apt-get --yes install -- "$@"
		fi
		TMPDIR=$(mktemp -d)
		(cd "$TMPDIR" && apt-get --yes -o APT::Sandbox::User=root download locales)
		dpkg --unpack "$TMPDIR"/locales_*.deb
		rm -rf "$TMPDIR"
		apt-mark auto -- "$@"
	fi
fi
if [ -z "$_LANG" ]; then
	if [ -e /usr/share/i18n/locales ]; then
		_LANGS=$(grep --with-filename --max-count=1 --perl-regexp --only-matching \
			--regexp='^language\s+"\K[^"]+' /usr/share/i18n/locales/*_* \
			| sed --regexp-extended \
				--expression='s,.*?/([a-z]+)_.*:(.*),\1\t\2,' \
				--expression='s,^az\t[^\t]+,az\tAzerbaijani,' \
				--expression='s,^de\t[^\t]+,de\tGerman,' \
				--expression='s,^en\t[^\t]+,en\tEnglish,' \
				--expression='s,^es\t[^\t]+,es\tSpanish,' \
				--expression='s,^fr\t[^\t]+,fr\tFrench,' \
				--expression='s,^nds\t[^\t]+,nds\tLow German,' \
				--expression='s,^nl\t[^\t]+,nl\tDutch,' \
				--expression='s,^pt\t[^\t]+,pt\tPortuguese,' \
				--expression='s,<U00E5>,å,' \
			| sort --key=1,1 --unique \
			| sort --key=2 \
			| cut --fields=1,2 --only-delimited)
	else
		_LANGS=$(find /usr/share/locale -maxdepth 1 -type d \
			| grep --perl-regexp --only-matching --regexp='/\K[a-z]{2,3}(?=_|\z)' \
			| sed --regexp-extended \
				--expression='s,^([^\t]+),\1\t\1,' \
			| sort --key=1,1 --unique \
			| cut --fields=1,2 --only-delimited)
	fi
	_LANG=$(ui_radiolist "Choose language" \
		'Choose the language to be used as the default language for the installed system.' \
		'Language:' \
		"$_LANGS" \
		"${_LANG:-en}" \
		'6' \
		'Enter language code:' \
		'none	No localization')
	[ none != "$_LANG" ] || _LANG=
fi
if [ -z "$_COUNTRY" ]; then
	_LANGCOUNTRIES=
	if [ -n "${_LANG}" ]; then
		_LANGCOUNTRIES=$(find /usr/share/i18n/locales/*_* /usr/share/locale -maxdepth 1 2>/dev/null \
			| grep --perl-regexp --only-matching --regexp='/'"${_LANG}"'_\K[A-Z]{2,3}(?!=[A-Z])' \
			| sort --unique)
	fi
	_COUNTRIES=$(grep --regexp='^[A-Z]' /usr/share/zoneinfo/iso3166.tab \
		| sort --key=2)
	_COUNTRY=$(ui_radiolist "Choose location" \
		'The selected location will be used to set your time zone and also for example to help select the system locale. Normally this should be the country where you live.' \
		'Country, territory or area:' \
		"$_COUNTRIES" \
		"${COUNTRY:-$(echo "$_LANGCOUNTRIES" | head --lines=1)}" \
		'5' \
		'Enter country/territory/area code:' \
		'none	None')
	[ none != "$_COUNTRY" ] || _COUNTRY=
fi
_CHANGELOCALE=
if [ -n "${_LANG}" ] && [ -n "${_COUNTRY}" ]; then
	_NEWLOCALE="${_LANG}_${_COUNTRY}.UTF-8"
	if [ "$_NEWLOCALE" != "$_LOCALE" ] && [ ! -f /etc/locale.gen ]; then
		_LOCALE="$_NEWLOCALE"
		_CHANGELOCALE=yes
	fi
	cat <<EOF | debconf-set-selections
d-i debian-installer/language string ${_LANG}
d-i debian-installer/country string ${_COUNTRY}
EOF
fi
if [ -n "$_CHANGELOCALE" ]; then
	echo "LANG=${_LOCALE}" > /etc/default/locale
	rm -f "/etc/locale.gen"
	cat <<EOF | debconf-set-selections
d-i debian-installer/locale string ${_LOCALE}
locales locales/default_environment_locale select ${_LOCALE}
locales locales/default_environment_locale seen false
locales locales/locales_to_be_generated multiselect ${_LOCALE} UTF-8
locales locales/locales_to_be_generated seen false
EOF
fi
if pkgstatus locales | grep --quiet --regexp=^iU; then
	if [ -z "$SKIP_NET" ]; then
		apt-get --yes install locales
	else
		dpkg --configure locales
	fi
elif [ -n "$_CHANGELOCALE" ] && pkgstatus locales | grep --quiet --regexp=^.i; then
	dpkg-reconfigure --priority=medium --unseen-only locales
fi
if command -v locale >/dev/null; then
    eval $(locale)
fi

# setup keyboard
if pkgstatus keyboard-configuration | grep --quiet --regexp=^.i; then
	dpkg-reconfigure --default-priority --unseen-only keyboard-configuration
fi

# offer to move system to alternative media if available
REINSTALL=
LVM=
newdisk=
bigdisks=
if [ -z "$SKIP_NET" ] && rootfs_not_finalized; then
	bigdisks=$(lsblk --paths --output NAME,SIZE --nodeps --noheadings --bytes \
		| grep --perl-regexp --only-matching --regexp='^\S+(?=\s+\d{10,}$)')
fi
if [ -n "$bigdisks" ] && [ "$disk" != "$bigdisks" ]; then
	_bigdisks=$(lsblk --paths --output NAME,SIZE --nodeps --noheadings --bytes \
		| grep --perl-regexp --only-matching --regexp='^\S+(?=\s+\d{10,}$)' \
		| sed --regexp-extended \
			--expression='s,^('"$disk"'),\1\tContinue here on device \1,' \
			--expression='s,^([^\t]+)$,\1\tReinstall to device \1,' \
		| sort --key=1,1 --unique \
		| cut --fields=1,2 --only-delimited)
	newdisk=$(ui_radiolist "Select where to install" \
		'You can either finalize setup on same disk it was booted from, or reinstall on another disk.' \
		'Continue here or reinstall on another disk?' \
		"$_bigdisks" \
		"$disk" \
		'1' \
		'Enter target disk device:')
	[ "$disk" != "$newdisk" ] || newdisk=
fi
if [ -n "$newdisk" ]; then
	REINSTALL=$(ui_radiolist "Select how to install" \
		'You can reinstall either with simple partitioning or using logical volume management (LVM), optionally encrypted.' \
		'How to reinstall?' \
		'plain	Simple partitioning
lvm	Resizable partitions (LVM)
luks	Full disk encrypted resizable partitions (LUKS+LVM)' \
		'plain')

	case "$REINSTALL" in
		lvm) LVM=1; TOOLS="$TOOLS rsync lvm2";;
		luks) LVM=1; TOOLS="$TOOLS rsync lvm2 cryptsetup";;
		*) TOOLS="$TOOLS rsync";;
	esac

	if ! ui_yesno 'Really reinstall?' \
		"You have chosen to reinstall onto disk $newdisk, which will COMPLETELY ERASE anything that may already be stored on that disk." \
		'Are you sure you want to continue this install?' \
		'' \
		'Continue' 'Cancel' \
		'1'
	then
		echo "Script cancelled. Please run system-setup again." >&2
		exit 1
	fi
	echo "INFO: Proceeding to reinstall onto disk $newdisk ..." >&2

	apt-get --yes install $TOOLS

	wipefs --all --quiet "$newdisk"
	printf 'label: dos \n grain: 2MiB \n 2MiB 500MiB L \n - + L' | sfdisk --quiet --wipe=always --wipe-partitions=always "$newdisk" 2>/dev/null
	newbootpart=$(partx --show --nr 1 --output UUID,SIZE --noheadings "$newdisk" \
		| grep --perl-regexp --only-matching --regexp='^\S+(?=\s+500M$)' \
		| xargs -I% findfs PARTUUID=%)
	newrootpart=$(partx --show --nr 2 --output UUID --noheadings "$newdisk" \
		| xargs -I% findfs PARTUUID=%)
	wipefs --all --quiet "$newbootpart"
	wipefs --all --quiet "$newrootpart"
	if [ luks = "$REINSTALL" ]; then
		newlukspart="$newrootpart"
		echo
		echo "Initializing full disk encryption of $newrootpart ..."
		cryptsetup --batch-mode --verify-passphrase luksFormat --type luks2 "$newrootpart"
		cryptsetup open --type luks2 "$newrootpart" luksroot
		newrootpart=/dev/mapper/luksroot
	fi
	if [ -n "$LVM" ]; then
		pvcreate "$newrootpart"
		vgcreate vg_sys "$newrootpart"
		# TODO: adapt sizes to memory and disk
		lvcreate --name lv_swap --size 3G --yes vg_sys
		lvcreate --name lv_root --size 4G --yes vg_sys
		lvcreate --name lv_home --size 2G --yes vg_sys
		newrootpart=/dev/mapper/vg_sys-lv_root
		newswappart=/dev/mapper/vg_sys-lv_swap
		newhomepart=/dev/mapper/vg_sys-lv_home
	fi
	mkfs.ext4 -q -b 4096 -E stride=2,stripe-width=1024 -L boot "$newbootpart"
	mkfs.ext4 -q -b 4096 -E stride=2,stripe-width=1024 -L root "$newrootpart"
	if [ -n "$LVM" ]; then
		mkswap --force "$newswappart" 2>/dev/null
		mkfs.ext4 -q -F -b 4096 -E stride=2,stripe-width=1024 -L home "$newhomepart"
	fi
	udevadm settle
	newbootuuid=$(lsblk --output UUID --nodeps --noheadings "$newbootpart")
	newrootuuid=$(lsblk --output UUID --nodeps --noheadings "$newrootpart")
	mount "$newrootpart" /mnt
	rsync --archive -HAX --one-file-system --info=progress2 --exclude="/boot/*",{${LVM:+"/home/*",}"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found"} / /mnt
	if [ -n "$LVM" ]; then
		fixate_mountpoint /mnt/ swap "$newswappart" "$(lsblk --output UUID --nodeps --noheadings "$newswappart")"
		fixate_mountpoint /mnt/ /home "$newhomepart" "$(lsblk --output UUID --nodeps --noheadings "$newhomepart")"
		mount "$newhomepart" /mnt/home
		rsync --archive -HAX --one-file-system --info=progress2 /home/* /mnt/home/
		umount /mnt/home
	fi
	newluksuuid=
	if [ luks = "$REINSTALL" ]; then
		newluksuuid=$(lsblk --output UUID --nodeps --noheadings "$newlukspart")
		echo "luksroot UUID=$newluksuuid none luks" >> /mnt/etc/crypttab
	fi
	fixate_mountpoint /mnt/ /boot "$newbootpart" "$newbootuuid"
	mount "$newbootpart" /mnt/boot
	rsync --archive -HAX --one-file-system --info=progress2 --no-relative /boot/* /mnt/boot/
	mount --bind /proc /mnt/proc
	mount --bind /sys /mnt/sys
	mount --bind /dev /mnt/dev
	chroot /mnt update-initramfs -u 2>/dev/null
	fixate_bootloader /mnt "$newdisk" "$newrootuuid" "$newluksuuid"
	umount /mnt/dev
	umount /mnt/sys
	umount /mnt/proc
	umount /mnt/boot
	fixate_mountpoint /mnt/ / "$newrootpart" "$newrootuuid"
	umount /mnt
	echo "System succesfully reinstalled to the disk $newdisk." >&2
	echo "Please power down, remove install disk, restart and run system-setup again." >&2
	exit 0
fi

rootuuid=
bootuuid=
if [ -n "$disk" ] && [ -n "$rootpart" ] && rootfs_not_finalized; then
	rootuuid=$(lsblk --output UUID --nodeps --noheadings "$rootpart")
	if [ -n "$bootpart" ]; then
		bootuuid=$(lsblk --output UUID --nodeps --noheadings "$bootpart")
		fixate_mountpoint / /boot "$bootpart" "$bootuuid"
	fi
	fixate_bootloader '' "$disk" "$rootuuid" ''
	fixate_mountpoint / / "$rootpart" "$rootuuid"
fi

if [ ! -e /etc/hosts ]; then
	_HOSTNAME=$(dnsdomainname --short)
	_HOSTNAME=$(ui_inputbox 'Configure the network' \
		'Please enter the hostname for this system.\n\nThe hostname is a single word that identifies your system to the network. If you don'"'"'t know what your hostname should be, consult your network administrator. If you are setting up your own home network, you can make something up here.' \
		'Hostname:' \
		'' \
		"$_HOSTNAME")
	if [ -n "${_HOSTNAME}" ]; then
		if command -v hostnamectl >/dev/null; then
			hostnamectl set-hostname "${_HOSTNAME}"
		else
			echo "${_HOSTNAME}" > /etc/hostname
		fi
		_DOMAIN=$(dnsdomainname --domain)
		_DOMAIN=$(ui_inputbox 'Configure the network' \
			'The domain name is the part of your Internet address to the right of your host name.  It is often something that ends in .com, .net, .edu, or .org.  If you are setting up a home network, you can make something up, but make sure you use the same domain name on all your computers.' \
			'Domain name:' \
			'' \
			"$_DOMAIN")
		cat <<EOF > /etc/hosts
127.0.0.1	localhost
127.0.1.1	${_DOMAIN:+$_HOSTNAME.$_DOMAIN }$_HOSTNAME

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF
	fi
fi

# set wireless regulatory domain
file=/etc/default/crda
if [ -e "$file" ]; then
	[ -z "$(find "$file".orig -mtime +1 2>/dev/null)" ] && ext=orig || ext=bak
	cp -a "$file" "$file.$ext"
else
	touch "$file.orig"
fi
if grep --quiet --regexp='^REGDOMAIN=' "$file" 2>/dev/null; then \
	sed --in-place --regexp-extended --expression='s,^(REGDOMAIN=).*,\1'"$_COUNTRY"',' "$file"
elif grep --quiet --regexp='^#?REGDOMAIN=' "$file" 2>/dev/null; then \
	sed --in-place --regexp-extended --expression='s,^#(REGDOMAIN=).*,\1'"$_COUNTRY"',' "$file"
else
	echo "REGDOMAIN=$_COUNTRY" >> "$file"
fi

dpkg-reconfigure --default-priority --unseen-only tzdata

# setup remaining to-be-configured packages if any
# TODO: show remaining tasks and ask to skip (except maybe console-data)
#debconf_owners=$(debconf-show --list-owners)
set -- $(find /var/lib/dpkg/info -type f -name '*.config' \
	-exec basename --suffix=.config {} +)
set -- $(pkgstatus "$@" \
	| grep --perl-regexp --only-matching --regexp='^.i\s+\K(?!(?:debconf|locales|tzdata|keyboard-configuration) )\S+')
dpkg-reconfigure --default-priority --unseen-only -- "$@"
sync

# upgrade packages
[ -n "$SKIP_NET" ] || apt-get --yes upgrade

# TODO: offer addons installation

echo 'INFO: Box setup configured succesfully!' >&2
